# Ejercicio 4: Gestion de ramas 

- [Ejercicio 1](#Ejercicio1)
- [Ejercicio 2](#Ejercicio1)
- [Ejercicio 3](#Ejercicio1)
- [Ejercicio 4](#Ejercicio1)
- [Ejercicio 5](#Ejercicio1)

---
# Ejercicio 1 

Creé la nueva rama de bibliografía con el comando: 

 Git granch (Nombre de la rama)
  
  Y para ver las ramas existentes puse el comando: 
  
git granch -av 

![Imagen1](./images/1.png "Img1" ) 

---
# Ejercicio 2

Creé una página web con el nombre de Capitulo4.html que tiene el siguiente contenido 

![Imagen2](./images/2.png "Img2" ) 

Al tener echa la página, guardaremos lo guardamos y enviaremos los cambios al Stating Area con el comando : 

Git add -A 

![Imagen3](./images/3.png "Img3" ) 

Y Haremos un commit con el comando; 

Git commit -m "Texto para la descripcion del commit"

![Imagen4](./images/4.png "Img4" )

Despues para ver los commit y las ramas pondremos el comando: 

Git log --graph --all --oneline 


![Imagen5](./images/5.png "Img5" )


--- 
# Ejercicio 3 

Para cambiar a la rama de bibliografía tendremos que poner el comando: 

Git checkout bibliografia 

![Imagen6](./images/6.png "Img6" )

Y crearemos la página web bibliografía.html 

![Imagen7](./images/7.png "Img7" )

Añadiremos los cambios a la Stating Area con el comando comentado anteriormente y realizaremos el commit. 

Y lo mostraremos con el comando 

Git log --graph --all --oneline

![Imagen8](./images/8.png "Img8" )

--- 
# Ejercicio 4 

Ahora Fusioné las ramas master y bibliografía que creé anteriormente, para ello tube que ir a la rama master con el comando: 

Git checkout master 
 
![Imagen9](./images/9.png "Img9" )

Para fusionar las ramas ponemos el comando: 

Git marge Nombredelarama 

![Imagen10](./images/10.png "Img10" )

Para mostrarlo pondremos el comando anterior: 

it log --graph --all --oneline

![Imagen11](./images/11.png "Img11" 

Para eliminar la rama bibliografia tendremos que poner el comando: 

Git branch -d Nombredelarama

Y mostraremos el historial de repositorio 

![Imagen12](./images/12.png "Img12" )

---

# Ejercicio 5 

Crearemos la rama bibliografia con el comando: 

Git branch nombredelarama

![Imagen13](./images/13.png "Img13" )

Y cambiaremos a la rama con el comando: 

git checkout nombredelarama 

![Imagen14](./images/14.png "Img14" )

Ahoracrearemos el fichero bibliografia.html 

![Imagen15](./images/15.png "Img15" )

Despue realizaremos un commit. 

![Imagen16](./images/16.png "Img16" )

Y nos cambiamos a la rama master con el comando mencionado anteriormente 

![Imagen17](./images/17.png "Img17" )

Seguidamente modificaremos el archivo html

![Imagen18](./images/18.png "Img18" )

Hacemos un commit enviando los datos a Stating Area con el comando

Git commit -a -m "Descripcion del commit "

![Imagen19](./images/19.png "Img19" )

A continuación haremos un merge para unificar las ramas 

![Imagen20](./images/20.png "Img20" )

Y nos saldrá un error y tendremos que decidir cual 

![Imagen21](./images/21.png "Img21" )

En mi caso quiero guardar las dos versiones del archivo 

![Imagen22](./images/22.png "Img22" )

Seguidamente Haremos un commit como mencionamos enteriormente 

![Imagen23](./images/23.png "Img23" )

Seguidamente veremos el historial de repositorios como hicimos anteriormente 

![Imagen24](./images/24.png "Img24")
